# Visorando-API

NodeJS/ExpresJS API that scraps visorando trails.

## Requeriments

- NodeJS >= 14.15.4

## How to use?

- Install the dependencies

```sh
npm run install
```

- Launch the server
```sh
node src/index.js
```
- Server listen to PORT 5000 by default

- Browse an available visorando city.

```sh
localhost:5000/herault
```
- (Will generate a JSON will data scrapped form https://www.visorando.com/randonnee-herault.html)