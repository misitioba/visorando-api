const express = require('express')
const puppeteer = require('puppeteer');
const sander = require('sander')
const path = p => require('path').join(process.cwd(), p);
const cheerio = require('cheerio');
const app = express();

app.get('/:name', (req, res) => {
    (async () => {
        let name = req.params.name || 'herault'
        await preloadPage(name)
        let html = await sander.readFile(path(`/data/${name}.html`))
        const $ = cheerio.load(html);
        let json = $('.rando-title-sansDetail').parent().toArray().map(r => {
            let title = $(r).find('a').text().replace(/(\r\n|\n|\r)/gm, "").trim();

            let infos = $(r).find('[title=Distance]').parent().contents().filter(function () {
                return this.nodeType === 3
            }).toArray().map(r => r.data.replace(/(\r\n|\n|\r)/gm, "").trim()).filter(r => !!r)
            let description = $(r).find('p').html()
            infos = {
                title,
                description,
                distance: infos[0],
                positiveSlope: infos[1],
                negativeSlope: infos[2],
                duration: infos[3],
                difficulty: infos[4],
                departFrom: infos[5].split(' - ')[0]
            }
            return infos
        })
        res.json(json)
    })();
})

async function preloadPage(name = 'herault') {
    if (await sander.exists(path(`data/${name}.html`))) {
        return;
    }
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(`https://www.visorando.com/randonnee-${name}.html`);
    const html = await page.$eval('body', e => e.outerHTML);
    await sander.writeFile(path(`data/${name}.html`), html)
    await browser.close();
}
app.listen(process.env.PORT || 5000, () => console.log(`READY AT PORT ${process.env.PORT || 5000}`))
